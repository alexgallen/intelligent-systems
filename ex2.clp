(deffacts BHinical
	(lista 23 14 56 33))

(defrule R1
(declare (salience 100))
?f <- (lista $?x ?z ?y $?w)
(test (< ?z ?y))
=>
(assert (lista $?x ?z ?y $?w)))


(defrule R2
(declare (salience 150))
?f <- (lista $?x ?z ?y $?w)
(test (>= ?z ?y))
=>
(assert (lista $?x ?z ?y $?w)))

(defrule final
(declare (salience 200))
(lista $?list)
=>
(halt))
