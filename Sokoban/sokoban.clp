(deftemplate game
    (multislot player)
    (multislot box)
    (slot level)
    (slot movement)
    (slot filled-warehouses)
    (slot fact))

(deftemplate obst
    (slot x)
    (slot y))

(deftemplate warehouse
    (slot x)
    (slot y))

(defglobal ?*rows* = 5)
(defglobal ?*cols* = 8)

(defrule right
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    ; Checks that player cant go out of grid
    (test (<> ?px ?*cols*))
    ; Checks that player cant move on obstacle or warehouse
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (+ ?px 1))))))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (+ ?px 1))))))
    ; Checks that player cant move on top of box
    
    (test (or (not(member$ (create$ (+ ?px 1) ?py) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ (+ ?px 1) ?py) $?b)) 2)))))

    ; Checks that player doesnt move back and forth
    (test (neq ?mov left))
    ; Checks that it doesnt continue after specified  level is reached
    (test (< ?level ?prof))
    =>
    (assert (game (player (+ ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement right) (filled-warehouses ?fv) (fact ?f)))
)

(defrule left
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)

    (test (<> ?px 1))

    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (- ?px 1))))))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (- ?px 1))))))

    (test (or (not(member$ (create$ (- ?px 1) ?py) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ (- ?px 1) ?py) $?b)) 2)))))

    (test (neq ?mov right))

    (test (< ?level ?prof))
    =>
    (assert (game (player (- ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement left) (filled-warehouses ?fv) (fact ?f)))
)

(defrule up
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (<> ?py 1))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (- ?py 1))))))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (- ?py 1))))))
    (test (or (not(member$ (create$ ?px (- ?py 1)) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ ?px (- ?py 1)) $?b)) 2)))))
    (test (neq ?mov down))
    (test (< ?level ?prof))
    =>
    (assert (game (player ?px (- ?py 1)) (box $?b) (level (+ ?level 1)) (movement up) (filled-warehouses ?fv) (fact ?f)))
)

(defrule down
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (<> ?py ?*rows*))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (+ ?py 1))))))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (+ ?py 1))))))
    (test (or (not(member$ (create$ ?px (+ ?py 1)) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ ?px (+ ?py 1)) $?b)) 2)))))
    (test (neq ?mov up))
    (test (< ?level ?prof))
    =>
    (assert (game (player ?px (+ ?py 1)) (box $?b) (level (+ ?level 1)) (movement down) (filled-warehouses ?fv) (fact ?f)))
)

(defrule box-right
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (< ?px (- ?*cols* 1)))
    ; Checks that slot after the box does not include obstacle
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (+ ?px 2))))))
    ; Checks that slot after the box does not include box
    
    (test (or(not(member$ (create$ (+ ?px 2) ?py) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ (+ ?px 2) ?py) $?b)) 2)))))
    ; Checks that there is a box on the right side of the player
    (test (member$ (create$ (+ ?px 1) ?py) $?b))
    (test (= 1 (mod (nth$ 1 (member$ (create$ (+ ?px 1) ?py) $?b)) 2)))
    ; Checks that there is not a warehouse on the right side of the player (box inside of warehouse)
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (+ ?px 1))))))

    (test (neq ?mov left))
    (test (< ?level ?prof))
    =>
    ; Loops as many times as list of boxes is long
    (loop-for-count (?cnt 1 (length$ $?b)) do
        ; Creates new loop for each odd value of the Counter (?cnt) (i.e. 1,3,5) to check x,y pairs of boxes
        (if (= (mod ?cnt 2) 1) then
            ; Checks if box pair matches player location in specified direction (i.e. for box-right: box (4,3) player (3,3))
            (if (eq (subseq$ $?b ?cnt (+ ?cnt 1)) (create$ (+ ?px 1) ?py)) then
                ; Moves the box, i.e. binds new values to box list
                (bind $?b (replace$ $?b ?cnt (+ ?cnt 1) (+ ?px 2) ?py))
                (do-for-all-facts
                    ((?w warehouse))
                    (and(= ?w:x (+ ?px 2)) (= ?w:y ?py))
                    (bind ?fv (+ ?fv 1))
                )
            )
        )
    )
    (bind ?test (nth$ 1 (member$ (create$ (+ ?px 1) ?py) $?b)))
    (bind $?b (replace$ $?b ?test (+ ?test 1) (+ ?px 2) ?py))
    ; Updates the fact with player and box movement
    (assert (game (player (+ ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement null) (filled-warehouses ?fv) (fact ?f)))
)

(defrule box-left
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (> ?px 2))
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (- ?px 2))))))
    ; Checks that slot after the box does not include box
    
    (test (or(not(member$ (create$ (- ?px 2) ?py) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ (- ?px 2) ?py) $?b)) 2)))))
    ; Checks that there is a box on the right side of the player
    (test (member$ (create$ (- ?px 1) ?py) $?b))
    (test (= 1 (mod (nth$ 1 (member$ (create$ (- ?px 1) ?py) $?b)) 2)))

    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (- ?px 1))))))
    (test (neq ?mov right))
    (test (< ?level ?prof))
    =>
    (loop-for-count (?cnt 1 (length$ $?b)) do
        (if (= (mod ?cnt 2) 1) then
            (if (eq (subseq$ $?b ?cnt (+ ?cnt 1)) (create$ (- ?px 1) ?py)) then
                (bind $?b (replace$ $?b ?cnt (+ ?cnt 1) (- ?px 2) ?py))
                (do-for-all-facts
                    ((?w warehouse))
                    (and(= ?w:x (- ?px 2)) (= ?w:y ?py))
                    (bind ?fv (+ ?fv 1))
                ) 
            )
        )
    )
    (assert (game (player (- ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement null) (filled-warehouses ?fv) (fact ?f)))
)

(defrule box-up
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (> ?py 2))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (- ?py 2))))))
    ; Checks that slot after the box does not include box
    
    (test (or(not(member$ (create$ ?px (- ?py 2)) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ ?px (- ?py 2)) $?b)) 2)))))
    ; Checks that there is a box on the right side of the player
    (test (member$ (create$ ?px (- ?py 1)) $?b))
    (test (= 1 (mod (nth$ 1 (member$ (create$ ?px (- ?py 1)) $?b)) 2)))

    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (- ?py 1))))))
    (test (neq ?mov down))
    (test (< ?level ?prof))
    =>
    (loop-for-count (?cnt 1 (length$ $?b)) do
        (if (= (mod ?cnt 2) 1) then
            (if (eq (subseq$ $?b ?cnt (+ ?cnt 1)) (create$ ?px (- ?py 1))) then
                (bind $?b (replace$ $?b ?cnt (+ ?cnt 1) ?px (- ?py 2))) 
                (do-for-all-facts
                    ((?w warehouse))
                    (and(= ?w:x ?px) (= ?w:y (- ?py 2)))
                    (bind ?fv (+ ?fv 1))
                )
            )
        )
    )

    (assert (game (player ?px (- ?py 1)) (box $?b) (level (+ ?level 1)) (movement null) (filled-warehouses ?fv) (fact ?f)))
)

(defrule box-down
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (max-depth ?prof)
    (test (< ?py (- ?*rows* 1)))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (+ ?py 2))))))
    ; Checks that slot after the box does not include box
    
    (test (or(not(member$ (create$ ?px (+ ?py 2)) $?b)) (not(= 1 (mod (nth$ 1 (member$ (create$ ?px (+ ?py 2)) $?b)) 2)))))
    ; Checks that there is a box on the right side of the player
    (test (member$ (create$ ?px (+ ?py 1)) $?b))
    (test (= 1 (mod (nth$ 1 (member$ (create$ ?px (+ ?py 1)) $?b)) 2)))

    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (+ ?py 1))))))
    (test (neq ?mov up))
    (test (< ?level ?prof))
    =>
    (loop-for-count (?cnt 1 (length$ $?b)) do
        (if (= (mod ?cnt 2) 1) then
            (if (eq (subseq$ $?b ?cnt (+ ?cnt 1)) (create$ ?px (+ ?py 1))) then
                (bind $?b (replace$ $?b ?cnt (+ ?cnt 1) ?px (+ ?py 2)))
                (do-for-all-facts
                    ((?w warehouse))
                    (and(= ?w:x ?px) (= ?w:y (+ ?py 2)))
                    (bind ?fv (+ ?fv 1))
                ) 
            )
        )
    )
    (assert (game (player ?px (+ ?py 1)) (box $?b) (level (+ ?level 1)) (movement null) (filled-warehouses ?fv) (fact ?f)))
)

(deffunction start ()
        (reset)
	(printout t "Maximum depth:= " )
	(bind ?prof (read))
	(printout t "Search strategy " crlf "    1.- Breadth" crlf "    2.- Depth" crlf )
	(bind ?a (read))
	(if (= ?a 1)
	       then    (set-strategy breadth)
	       else   (set-strategy depth))
    (printout t " Execute run to start the program " crlf)
 
    (assert (warehouse (x 7) (y 1))
    (warehouse (x 5) (y 4))
    (warehouse (x 5) (y 5))
    (obst  (x 4) (y 1))
    (obst  (x 1) (y 3))
    (obst  (x 4) (y 3))
    (obst  (x 5) (y 3))
    (obst  (x 8) (y 3))
    (obst  (x 4) (y 4))
    (obst  (x 4) (y 5))
	(max-depth ?prof)
    (game (player 1 4) (box 2 2 6 2 3 4) (level 0) (movement null) (filled-warehouses 0) (fact 0)))
)

(defrule goal
    (declare (salience 100))
    (game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) (filled-warehouses ?fv) (fact ?))
    (test (eq ?fv (length$ (find-all-facts ((?w warehouse)) TRUE))))
    =>
    (printout t "VICTORY!" crlf)
    (halt)
)