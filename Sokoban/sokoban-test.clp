; GROUP MEMBERS: ALEXANDER GALLEN & LASSE HEIA
(deftemplate game
    (multislot player)
    (multislot box)
    (slot level)
    (slot movement)
    (slot fact))

(deftemplate obst
    (slot x)
    (slot y))

(deftemplate warehouse
    (slot x)
    (slot y))

(defglobal ?*rows* = 5)
(defglobal ?*cols* = 8)

(defrule right
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    ; Checks that player cant go out of grid
    (test (< ?px ?*cols*))
    ; Checks that player doesnt move back and forth
    (test (neq ?mov left))
    ; Checks that it doesnt continue after specified  level is reached
    (test (< ?level ?prof))
    ; Checks that player cant move on obstacle or warehouse
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (+ ?px 1))))))
    ; Checks that player cant move on top of box
    (test (not(member$ (create$ (+ ?px 1) ?py) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (+ ?px 1))))))
    =>
    (assert (game (player (+ ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement right) ))
)

(defrule left
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (> ?px 1))
    (test (neq ?mov right))
    (test (< ?level ?prof))
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (- ?px 1))))))
    (test (not(member$ (create$ (- ?px 1) ?py) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (- ?px 1))))))
    =>
    (assert (game (player (- ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement left) ))
)

(defrule up
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (> ?py 1))
    (test (neq ?mov down))
    (test (< ?level ?prof))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (- ?py 1))))))
    (test (not(member$ (create$ ?px (- ?py 1)) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (- ?py 1))))))
    =>
    (assert (game (player ?px (- ?py 1)) (box $?b) (level (+ ?level 1)) (movement up) ))
)

(defrule down
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (< ?py ?*rows*))
    (test (neq ?mov up))
    (test (< ?level ?prof))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (+ ?py 1))))))
    (test (not(member$ (create$ ?px (+ ?py 1)) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (+ ?py 1))))))
    =>
    (assert (game (player ?px (+ ?py 1)) (box $?b) (level (+ ?level 1)) (movement down) ))
)

(defrule box-right
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (< ?px (- ?*cols* 1)))
    (test (neq ?mov left))
    (test (< ?level ?prof))
    ; Checks that there is a box on the right side of the player
    (test (member$ (create$ (+ ?px 1) ?py) $?b))
    ; Checks that slot after the box does not include obstacle
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (+ ?px 2))))))
    ; Checks that slot after the box does not include box
    (test (not(member$ (create$ (+ ?px 2) ?py) $?b)))
    ; Checks that there is not a warehouse on the right side of the player (box inside of warehouse)
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (+ ?px 1))))))
    =>
    (bind $?b (replace-member$ $?b (create$ (+ ?px 2) ?py x) (create$ (+ ?px 1) ?py x)))
    ; Updates the fact with player and box movement
    (assert (game (player (+ ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement null) ))
)

(defrule box-left
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (> ?px 2))
    (test (neq ?mov right))
    (test (< ?level ?prof))
    (test (member$ (create$ (- ?px 1) ?py) $?b))
    (test (not(any-factp ((?o obst)) (and(= ?o:y ?py) (= ?o:x (- ?px 2))))))
    (test (not(member$ (create$ (- ?px 2) ?py) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:y ?py) (= ?w:x (- ?px 1))))))
    =>
    (bind $?b (replace-member$ $?b (create$ (- ?px 2) ?py x) (create$ (- ?px 1) ?py x)))
    (assert (game (player (- ?px 1) ?py) (box $?b) (level (+ ?level 1)) (movement null) ))
)

(defrule box-up
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (> ?py 2))
    (test (neq ?mov down))
    (test (< ?level ?prof))
    (test (member$ (create$ ?px (- ?py 1)) $?b))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (- ?py 2))))))
    (test (not(member$ (create$ ?px (- ?py 2)) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (- ?py 1))))))
    =>
    (bind $?b (replace-member$ $?b (create$ ?px (- ?py 2) x) (create$ ?px (- ?py 1) x)))
    (assert (game (player ?px (- ?py 1)) (box $?b) (level (+ ?level 1)) (movement null) ))
)

(defrule box-down
    ?f<-(game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (max-depth ?prof)
    (test (< ?py (- ?*rows* 1)))
    (test (neq ?mov up))
    (test (< ?level ?prof))
    (test (member$ (create$ ?px (+ ?py 1)) $?b))
    (test (not(any-factp ((?o obst)) (and(= ?o:x ?px) (= ?o:y (+ ?py 2))))))
    (test (not(member$ (create$ ?px (+ ?py 2)) $?b)))
    (test (not(any-factp ((?w warehouse)) (and(= ?w:x ?px) (= ?w:y (+ ?py 1))))))
    =>
    (bind $?b (replace-member$ $?b (create$ ?px (+ ?py 2) x) (create$ ?px (+ ?py 1) x)))
    (assert (game (player ?px (+ ?py 1)) (box $?b) (level (+ ?level 1)) (movement null) ))
)

(deffunction start ()
        (reset)
	(printout t "Maximum depth:= " )
	(bind ?prof (read))
	(printout t "Search strategy " crlf "    1.- Breadth" crlf "    2.- Depth" crlf )
	(bind ?a (read))
	(if (= ?a 1)
	       then    (set-strategy breadth)
	       else   (set-strategy depth))
    (printout t " Execute run to start the program " crlf)
 
    (assert (warehouse (x 7) (y 1))
    (warehouse (x 5) (y 4))
    (warehouse (x 5) (y 5))
    (obst  (x 4) (y 1))
    (obst  (x 1) (y 3))
    (obst  (x 4) (y 3))
    (obst  (x 5) (y 3))
    (obst  (x 8) (y 3))
    (obst  (x 4) (y 4))
    (obst  (x 4) (y 5))
	(max-depth ?prof)
    (game (player 1 4) (box 2 2 x 6 2 x 3 4 x) (level 0) (movement null) ))
)

(defrule goal
    (declare (salience 100))
    (game (player ?px ?py) (box $?b) (level ?level) (movement ?mov) )
    (test (= (length$ (find-all-facts ((?w warehouse)) TRUE)) (length$ (find-all-facts ((?w warehouse)) (member$ (create$ ?w:x ?w:y) $?b)))))
    =>
    (printout t "VICTORY!" crlf)
    (halt)
)